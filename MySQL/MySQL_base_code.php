<?php
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "image_uploads";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$result = $conn->query("SELECT name, path FROM images");

echo "<h2>Uploaded Images</h2>";

while ($row = $result->fetch_assoc()) {
    echo '<div>';
    echo '<h3>' . $row['name'] . '</h3>';
    echo '<img src="' . $row['path'] . '" width="200" />';
    echo '</div>';
}

$conn->close();
?>
