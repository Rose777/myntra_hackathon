from keras import models
from keras import layers
from keras import optimizers
from keras.applications import MobileNet
from keras.preprocessing.image import ImageDataGenerator
import os

conv_base = MobileNet(weights='imagenet',
                  include_top=False,
                  input_shape=(224, 224, 3))

model = models.Sequential()
model.add(conv_base)
model.add(layers.Flatten())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dense(7, activation='sigmoid'))

model.summary()

conv_base.trainable = True

model.compile(optimizer=optimizers.RMSprop(lr=2e-5),
              loss='categorical_crossentropy',
              metrics=['acc'])
conv_base.summary()

sample_body_images = '../../data/'

input_body_image = os.path.join(sample_body_images, 'train')
validation_dir = os.path.join(sample_body_images, 'validation')
train_datagen = ImageDataGenerator(rescale=1./255)
validation_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
        
        input_body_image,
        #Label box Input leni he
        batch_size=20,
        class_mode='categorical')

validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(224, 224),
        batch_size=20,
        class_mode='categorical')

history = model.fit_generator(
      train_generator,
      steps_per_epoch=100,
      epochs=100,
      validation_data=validation_generator,
      validation_steps=50,
      verbose=2)



model.save('init_weights_V1.0.h5')