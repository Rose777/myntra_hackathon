# Myntra_Hackathon

 
## Overview

This project aims to develop an AI system for determining body figures and analyzing clothing trends from images.It will connect the post and video algorithm from social media sites to put forward similiar body type celebrities for the user. The primary goal is to provide personalized clothing recommendations based on the detected body type and according fashion trends using a Ai model for a better experience on online shopping

## Name
FitAi-Ai intsallation 

## Features
- [ ] Body Detection and Classification: Detect and classify different body figures using deep learning models
- [ ] Trend Analysis: Analyze fashion trends from a dataset of images and recommend clothing styles.
- [ ] Personalized Recommendations: Provide personalized clothing options based on body type and current trends.
- [ ] Real-time Processing: Support for real-time image processing and recommendations.

## Installation

- [ ] Python 3.7 or higher
- [ ] TensorFlow 2.x
- [ ] Pillow
- [ ] MySQL
- [ ] PyTorch

## Feasibility


- [ ] User Acceptance:
- Personalized fashion recommendations have a high potential for user engagement and satisfaction.


- [ ] Market Demand:
- The fashion industry is increasingly leveraging AI for personalization, indicating a growing market demand.

- [ ] Cost:
- Initial development costs can be minimized using open-source tools and pre-trained models.


## Contact
For any doubt or enquiry reach out at [chatterjisayanti@gmail.com)

