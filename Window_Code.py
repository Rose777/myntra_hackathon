from tkinter import *
from PIL import ImageTk,Image
#import pymysql
from tkinter import messagebox


root = Tk()
root.title("Myntra_app_demo")
root.minsize(width=400,height=400)
root.geometry("600x500")

same=True
n=1.10

background_image =Image.open("Myn1.jpg")
[imageSizeWidth, imageSizeHeight] = background_image.size

newImageSizeWidth = int(imageSizeWidth*n)
if same:
    newImageSizeHeight = int(imageSizeHeight*n) 
else:
    newImageSizeHeight = int(imageSizeHeight/n) 
    
background_image = background_image.resize((newImageSizeWidth,newImageSizeHeight))
img = ImageTk.PhotoImage(background_image)

Canvas1 = Canvas(root)

Canvas1.create_image(700,350,image = img)      
Canvas1.config(bg="white",width = newImageSizeWidth, height = newImageSizeHeight)
Canvas1.pack(expand=True,fill=BOTH)

btn = Button(root,text="FitAi",bg='magenta', fg='black', font=('Helvetica', ))
btn.place(relx=0.9,rely=0.9, relwidth=0.05,relheight=0.05)

root.mainloop()
